#pragma once
const float SCALE_FACTOR = 2.0f;
const float PLAYER_MAX_VELOCITY = 10.0f;
const float PLAYER_JUMP_VELOCITY = 40.0f;
const float DAMPING = 0.87f;