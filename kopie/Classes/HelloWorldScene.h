#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#include "cocos2d.h"
#include "Level.h"
#include "Player.h"
class HelloWorld : public cocos2d::Layer
{
public:
  
	Level * level;
	Player *player;
	Sprite *player_sprite;


	static cocos2d::Scene* createScene();

	virtual bool init();

	void menuCloseCallback(cocos2d::Ref* pSender);

	CREATE_FUNC(HelloWorld);

	HelloWorld(void);
	virtual ~HelloWorld(void);
};

#endif // __HELLOWORLD_SCENE_H__
