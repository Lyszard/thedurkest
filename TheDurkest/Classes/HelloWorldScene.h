

#pragma once
#include "cocos2d.h"
#include"Player.h"
#include <vector>
#include <string>
#include <algorithm>
#include"Level.h"
#include"Enemy.h"
using namespace std;
using namespace cocos2d;

class HelloWorld : public Layer
{
public:
	static Scene* createScene();
	int FPSCounter;
	vector<EventKeyboard::KeyCode> heldKeys;
	

	int gold;
	int PDMG;
	int EDMG;


	bool dead;
	void IsPlayerNearby();
	void FollowPlayer(int i);
	void AttackCollision();
	void PlayerDMGDetection();
	virtual void onKeyPressed(EventKeyboard::KeyCode keyCode, Event * event);
	virtual void onKeyReleased(EventKeyboard::KeyCode keyCode, Event * event);
	virtual bool init();
	void spriteMoveFinished(Node * sender);
	virtual void updateScene(float dt);
	CREATE_FUNC(HelloWorld);
	HelloWorld(void);
	virtual ~HelloWorld(void);
private:
	//CocosDenshion::SimpleAudioEngine * background;
	Player * player_sprite;
	Vector <Enemy * > ALLENEMIES;
	Enemy * Enemy_sprite;
	Level * level;
	Follow *camera;
	Sprite *CameraTarget;
	Sprite * Background;
	int AnimHandler;
	int EnemyFollowCheck;

	
};



