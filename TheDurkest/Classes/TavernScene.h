#pragma once

#include "cocos2d.h"
#include"parameters.h"
#include <vector>
#include <string>
#include <algorithm>
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"
#include "parameters.h"
#include<iostream>
#include<fstream>
#include"TownScene.h"



using namespace std;
class TavernScene : public cocos2d::Layer
{
public:
	// there's no 'id' in cpp, so we recommend returning the class instance pointer
	static cocos2d::Scene* createScene();

	// Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
	virtual bool init();
	vector<EventKeyboard::KeyCode> heldKeys;
	// a selector callback
	void menuCloseCallback(cocos2d::Ref* pSender);
	virtual void onKeyPressed(EventKeyboard::KeyCode keyCode, Event * event);
	// implement the "static create()" method manually
	CREATE_FUNC(TavernScene);

	cocos2d::Sprite *mySprite;

	void Play(Ref *pSender);
	void Highscores(Ref *pSender);
	void Settings(Ref *pSender);

	void ImageButton(Ref *pSender);

};

