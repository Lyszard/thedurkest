#include "TavernScene.h"
#include "HelloWorldScene.h"
#include<vector>
USING_NS_CC;

Scene* TavernScene::createScene()
{
	// 'scene' is an autorelease object
	auto scene = Scene::create();

	// 'layer' is an autorelease object
	auto layer = TavernScene::create();

	// add layer as a child to scene
	scene->addChild(layer);

	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
bool TavernScene::init()
{
	//////////////////////////////
	// 1. super init first
	if (!Layer::init())
	{
		return false;
	}

	Size visibleSize = Director::getInstance()->getVisibleSize();
	Point origin = Director::getInstance()->getVisibleOrigin();

	mySprite = Sprite::create("TavernInside.jpg");
	mySprite->setScale(0.9);


	mySprite->setPosition(Point((visibleSize.width / 2) + origin.x, (visibleSize.height / 2) + origin.y));

	this->addChild(mySprite);

	/*auto menu_item_1 = MenuItemFont::create("Play", CC_CALLBACK_1(TavernScene::Play, this));
	auto menu_item_2 = MenuItemFont::create("Highscores", CC_CALLBACK_1(TavernScene::Highscores, this));
	auto menu_item_3 = MenuItemFont::create("Settings", CC_CALLBACK_1(TavernScene::Settings, this));*/
	auto menu_item_4 = MenuItemImage::create("QuestIcon.png", "QuestIcon.png", CC_CALLBACK_1(TavernScene::ImageButton, this));

	/*menu_item_1->setPosition(Point(visibleSize.width / 2, (visibleSize.height / 5) * 4));
	menu_item_2->setPosition(Point(visibleSize.width / 2, (visibleSize.height / 5) * 3));
	menu_item_3->setPosition(Point(visibleSize.width / 2, (visibleSize.height / 5) * 2));*/
	menu_item_4->setPosition(Point(visibleSize.width / 3.3, (visibleSize.height / 3.1) ));
	menu_item_4->setScale(0.5);


	auto *menu = Menu::create(menu_item_4, NULL);
	menu->setPosition(Point(0, 0));
	this->addChild(menu);


	return true;
}
void TavernScene::onKeyPressed(EventKeyboard::KeyCode keyCode, Event * event)
{
	if (std::find(heldKeys.begin(), heldKeys.end(), keyCode) == heldKeys.end())
	{
		heldKeys.push_back(keyCode);

	}
	if (std::find(heldKeys.begin(), heldKeys.end(), EventKeyboard::KeyCode::KEY_ESCAPE) != heldKeys.end())
	{

		Director::getInstance()->popScene();
	}
}
void TavernScene::Play(cocos2d::Ref *pSender)
{
	CCLOG("Play");
}

void TavernScene::Highscores(cocos2d::Ref *pSender)
{
	CCLOG("Highscores");
	
}

void TavernScene::Settings(cocos2d::Ref *pSender)
{
	CCLOG("Settings");
}

void TavernScene::ImageButton(cocos2d::Ref *pSender)
{
	CCLOG("IMAGE Button");
	auto scene = HelloWorld::createScene();
	Director::getInstance()->pushScene(scene);
}

void TavernScene::menuCloseCallback(Ref* pSender)
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WP8) || (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)
	MessageBox("You pressed the close button. Windows Store Apps do not implement a close button.", "Alert");
	return;
#endif

	Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	exit(0);
#endif
}