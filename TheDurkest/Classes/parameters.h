#pragma once

#include "cocos2d.h"
using namespace cocos2d;

//bind for keyboard
const EventKeyboard::KeyCode ENTER = EventKeyboard::KeyCode::KEY_ENTER;
const EventKeyboard::KeyCode LEFT_ARROW = EventKeyboard::KeyCode::KEY_LEFT_ARROW;
const EventKeyboard::KeyCode RIGHT_ARROW = EventKeyboard::KeyCode::KEY_RIGHT_ARROW;
const EventKeyboard::KeyCode SPACE = EventKeyboard::KeyCode::KEY_SPACE;
const EventKeyboard::KeyCode ESC = EventKeyboard::KeyCode::KEY_ESCAPE;
const float SCALE_FACTOR = 3.0f;
