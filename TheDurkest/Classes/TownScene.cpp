#include "TownScene.h"
#include "TavernScene.h"
#include<iostream>
#include<fstream>
USING_NS_CC;

Scene* TownScene::createScene()
{
	// 'scene' is an autorelease object
	auto scene = Scene::create();

	// 'layer' is an autorelease object
	auto layer = TownScene::create();

	// add layer as a child to scene
	scene->addChild(layer);

	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
bool TownScene::init()
{
	//////////////////////////////
	// 1. super init first
	if (!Layer::init())
	{
		return false;
	}

	Size visibleSize = Director::getInstance()->getVisibleSize();
	Point origin = Director::getInstance()->getVisibleOrigin();

	//mySprite = Sprite::create("CloseNormal.png");

	//mySprite->setPosition(Point((visibleSize.width / 2) + origin.x, (visibleSize.height / 2) + origin.y));

	//this->addChild(mySprite);

	/*auto menu_item_1 = MenuItemFont::create("Play", CC_CALLBACK_1(TownScene::Play, this));
	auto menu_item_2 = MenuItemFont::create("Highscores", CC_CALLBACK_1(TownScene::Highscores, this));
	auto menu_item_3 = MenuItemFont::create("Settings", CC_CALLBACK_1(TownScene::Settings, this));*/
	auto menu_item_4 = MenuItemImage::create("Tavern.png", "Tavern.png", CC_CALLBACK_1(TownScene::ImageButton, this));

	/*menu_item_1->setPosition(Point(visibleSize.width / 2, (visibleSize.height / 5) * 4));
	menu_item_2->setPosition(Point(visibleSize.width / 2, (visibleSize.height / 5) * 3));
	menu_item_3->setPosition(Point(visibleSize.width / 2, (visibleSize.height / 5) * 2));*/
	menu_item_4->setPosition(Point(visibleSize.width / 2, (visibleSize.height / 2) * 1));

	auto *menu = Menu::create(menu_item_4, NULL);
	menu->setPosition(Point(0, 0));
	this->addChild(menu);


	return true;
}

void TownScene::Play(cocos2d::Ref *pSender)
{
	CCLOG("Play");
}

void TownScene::Highscores(cocos2d::Ref *pSender)
{
	CCLOG("Highscores");
}

void TownScene::Settings(cocos2d::Ref *pSender)
{
	CCLOG("Settings");
}

void TownScene::ImageButton(cocos2d::Ref *pSender)
{
	CCLOG("IMAGE Button");
	std::fstream save;
	save.open("GAMESAVE.txt", std::ios::out);
	gold = 0;
	PDMG = 10;
	EDMG = 5;
	save << gold << std::endl;
	save << PDMG << std::endl;
	save << EDMG << std::endl;
	save.close();
	auto scene = TavernScene::createScene();
	Director::getInstance()->pushScene(scene);
}

void TownScene::menuCloseCallback(Ref* pSender)
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WP8) || (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)
	MessageBox("You pressed the close button. Windows Store Apps do not implement a close button.", "Alert");
	return;
#endif

	Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	exit(0);
#endif
}