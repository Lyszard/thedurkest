#include "HelloWorldScene.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"
#include "parameters.h"
#include<iostream>
#include<fstream>
#include"TownScene.h"
#include "SimpleAudioEngine.h"
USING_NS_CC;

using namespace cocostudio::timeline;






Scene* HelloWorld::createScene()
{
	auto scene = Scene::create();
	
	
	auto layer = HelloWorld::create();
	scene->addChild(layer);
	
	return scene;
}

bool HelloWorld::init()
{
	if (!Layer::init())
	{
		return false;
	}
	std::fstream save;
	save.open("GAMESAVE.txt", std::ios::in);
	string line;
	int lineNR=1;

	while (std::getline(save, line))
	{

		switch (lineNR)
		{
		case 1 :
			gold = atoi(line.c_str());
			break;
		case 2:
			PDMG = atoi(line.c_str());
			break;
		case 3:
			EDMG = atoi(line.c_str());
			break;
		}
		lineNR++;

	}

	save.close();

	CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic("CalmingDungeons.mp3", true);
	//background = CocosDenshion::SimpleAudioEngine::getInstance();
	//background->playBackgroundMusic("CalmingDungeons.mp3", true);

	



	
	level = new Level();
	level->loadMap("mojamapa3.tmx");
	level->retain();
	EnemyFollowCheck = -1;
	auto director = Director::getInstance();
	level->getMap()->setScale(0.8);
	
	this->addChild(level->getMap());

	
	ALLENEMIES.pushBack(new Enemy(this, level, 1955 ));
	ALLENEMIES.pushBack(new Enemy(this, level, 3195));
	ALLENEMIES.pushBack(new Enemy(this, level, 1200));
	

	player_sprite = new Player(this, level);
	this->schedule(schedule_selector(HelloWorld::updateScene));

	Point origin = Director::getInstance()->getVisibleOrigin();
	Size wsize = Director::getInstance()->getVisibleSize();
	CameraTarget = Sprite::create();
	CameraTarget->setPositionX(player_sprite->player_sprite->getPosition().x); // set to players x
	CameraTarget->setPositionY(wsize.height / 2 + origin.y); // center of height

	CameraTarget->retain();


	Background = Sprite::create("BG.png");
	Background->setScale(0.7);
	Background->setZOrder(-3);
	Background->setPositionY(300);
	this->addChild(CameraTarget);
	this->addChild(Background);
	camera = Follow::create(CameraTarget, Rect::ZERO);
	camera->retain();

	this->runAction(camera);





	
	return true;
}


void HelloWorld::updateScene(float dt)
{
	//CocosDenshion::SimpleAudioEngine::getInstance()->stopAllEffects();
	CameraTarget->setPositionX(player_sprite->player_sprite->getPositionX());
	Background->setPositionX(player_sprite->player_sprite->getPositionX());
	PlayerDMGDetection();
	this->FPSCounter += 1;
	//CCLOG("%f", player_sprite->player_sprite->getPositionX());
	IsPlayerNearby();
	if (FPSCounter >= 120)
	{
		FPSCounter = 0;
		switch (EnemyFollowCheck)
		{
		case -1:
			for (int i = 0; i < 3; i++)
			{
				ALLENEMIES.at(i)->MoveEnemy();
			}
			break;
		case 0:
			ALLENEMIES.at(1)->MoveEnemy();
			ALLENEMIES.at(2)->MoveEnemy();
			break;
		case 1:
			ALLENEMIES.at(0)->MoveEnemy();
			ALLENEMIES.at(2)->MoveEnemy();
			break;
		case 2:
			ALLENEMIES.at(0)->MoveEnemy();
			ALLENEMIES.at(1)->MoveEnemy();
			break;
		default:
			break;
		}

		/*if (player_sprite->player_sprite->getPositionX() < 10)
			player_sprite->velocity_x = 10;*/
		
		
	}
	if (player_sprite->health <= 0)
	{
		if (dead == false)
		{
			player_sprite->DeathAnimation();
			setKeyboardEnabled(false);
			dead = true;
		}
		

	}
	for (int i = 0; i < 3; i++)
	{

		if (ALLENEMIES.at(i)->health <= 0)
		{
			CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
			CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("coin.wav", false);
			CocosDenshion::SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
			gold += 20;
			ALLENEMIES.at(i)->Enemy_sprite->stopAllActions();
			ALLENEMIES.at(i)->velocity_x = 0;
			if (ALLENEMIES.at(i)->dead == false)
			{
				ALLENEMIES.at(i)->DeathAnimation();

				ALLENEMIES.at(i)->dead = true;
				this->removeChildByTag(66);
			}


		}
	}
	
	if (FPSCounter >= 30&& player_sprite->atack==true)
	{
		
	}
		
	switch (EnemyFollowCheck)
	{
	case -1:
		break;
	case 0:
		this->FollowPlayer(0);
		break;
	case 1:
		this->FollowPlayer(1);
		break;
	case 2:
		this->FollowPlayer(2);
		break;
	case 3:
		this->FollowPlayer(3);
		break;
	default:
		break;
	}
	
	
	
	if (std::find(heldKeys.begin(), heldKeys.end(), EventKeyboard::KeyCode::KEY_RIGHT_ARROW) != heldKeys.end())
	{
		//CCLOG("%d",player_sprite->GetPosX());
		player_sprite->right = true;
		player_sprite->velocity_x = 5;
		player_sprite->MovePlayer(dt);
		if (player_sprite->player_sprite->getPositionX() > 3600)
		{
			
			player_sprite->velocity_x = 0;
			std::fstream save;
			save.open("GAMESAVE.txt", std::ios::out);
			save << gold << std::endl;
			save << PDMG << std::endl;
			save << EDMG << std::endl;
			save.close();
			Director::getInstance()->popScene();
		}
		AnimHandler = 1;
		
	}
	if (std::find(heldKeys.begin(), heldKeys.end(), EventKeyboard::KeyCode::KEY_LEFT_ARROW) != heldKeys.end())
	{
		

			
		player_sprite->right = false;
		player_sprite->velocity_x = -5;

		if (player_sprite->player_sprite->getPositionX() < 10)
		{
			player_sprite->velocity_x = 0;
		}



		player_sprite->MovePlayer(dt);
		AnimHandler = 1;
	}
	
	if (std::find(heldKeys.begin(), heldKeys.end(), EventKeyboard::KeyCode::KEY_ESCAPE) != heldKeys.end())
	{
		
		Director::getInstance()->popScene();
	}

	if (std::find(heldKeys.begin(), heldKeys.end(), EventKeyboard::KeyCode::KEY_F) != heldKeys.end())
	{

		
		//player_sprite->Atack(this);

	}
}



void HelloWorld::onKeyPressed(EventKeyboard::KeyCode keyCode, Event * event)
{
	if (std::find(heldKeys.begin(), heldKeys.end(), keyCode) == heldKeys.end())
	{
		heldKeys.push_back(keyCode);

	}
	if (std::find(heldKeys.begin(), heldKeys.end(), EventKeyboard::KeyCode::KEY_LEFT_ARROW) != heldKeys.end())
	{
		
		player_sprite->RunAnimation(this);
		//AnimHandler = 1;
	}
	if (std::find(heldKeys.begin(), heldKeys.end(), EventKeyboard::KeyCode::KEY_RIGHT_ARROW) != heldKeys.end())
	{

		player_sprite->RunAnimation(this);
		//AnimHandler = 1;
	}

	if (std::find(heldKeys.begin(), heldKeys.end(), EventKeyboard::KeyCode::KEY_F) != heldKeys.end())
	{
		//AnimHandler = 1;
		CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
		CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("swing3.wav", false);
		CocosDenshion::SimpleAudioEngine::getInstance()->resumeBackgroundMusic();

		player_sprite->Atack(this);
		player_sprite->AttackAnimation(this);
		player_sprite->atack = true;
		AttackCollision();
		//AnimHandler = 0;
	}
}

void HelloWorld::onKeyReleased(EventKeyboard::KeyCode keyCode, Event * event)
{
	heldKeys.erase(std::remove(heldKeys.begin(), heldKeys.end(), keyCode), heldKeys.end());
	player_sprite->velocity_x = 0;
	//this->removeChildByTag(9);
	player_sprite->IdleAnimation(this);
	
	this->removeChildByTag(9);
	
	//AnimHandler=
}
void HelloWorld::PlayerDMGDetection()
{

	for (int i = 0; i < 3; i++)
	{
		if (player_sprite->player_sprite->getBoundingBox().intersectsRect(ALLENEMIES.at(i)->Enemy_sprite->getBoundingBox()))
		{
			if (ALLENEMIES.at(i)->dead==false)
			{
				CCLOG("uderzonoMNIEE");
				player_sprite->health -= EDMG;
				CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
				CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("ogre5.wav", false);
				CocosDenshion::SimpleAudioEngine::getInstance()->resumeBackgroundMusic();

				if (player_sprite->right == true)
				{
					auto jumpback = MoveBy::create(0.1, Vec2(-20, 0));
					player_sprite->player_sprite->runAction(jumpback);
				}
				else if (ALLENEMIES.at(i)->right == false)
				{
					auto jumpback = MoveBy::create(0.1, Vec2(20, 0));
					player_sprite->player_sprite->runAction(jumpback);
				}
			}
		}
	}
}
void HelloWorld::AttackCollision()
{
	for (int i = 0; i < 3; i++)
	{

		//if (player_sprite->atack_sprite->getTextureRect().intersectsRect(ALLENEMIES.at(i)->Enemy_sprite->getTextureRect()))

		if(player_sprite->atack_sprite->getBoundingBox().intersectsRect(ALLENEMIES.at(i)->Enemy_sprite->getBoundingBox()))
		{
			CCLOG("uderzono");
			ALLENEMIES.at(i)->health -= PDMG;
			CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
			CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("shade9.wav", false);
			CocosDenshion::SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
			if (ALLENEMIES.at(i)->left == true)
			{
				auto jumpback =MoveBy::create(0.1, Vec2(100, 0));
				ALLENEMIES.at(i)->Enemy_sprite->runAction(jumpback);
			}
			else if(ALLENEMIES.at(i)->right==true)
			{
				auto jumpback = MoveBy::create(0.1, Vec2(-100, 0));
				ALLENEMIES.at(i)->Enemy_sprite->runAction(jumpback);
			}
		}
		/*if (player_sprite->player_sprite->getBoundingBox().intersectsRect(ALLENEMIES.at(i)->Enemy_sprite->getBoundingBox()))
		{
			CCLOG("uderzonoMNIEE");
			player_sprite->health-= EDMG;
			if (player_sprite->right == true)
			{
				auto jumpback = MoveBy::create(0.1, Vec2(-100, 0));
				player_sprite->player_sprite->runAction(jumpback);
			}
			else if (ALLENEMIES.at(i)->right == false)
			{
				auto jumpback = MoveBy::create(0.1, Vec2(100, 0));
				player_sprite->player_sprite->runAction(jumpback);
			}
		}*/
	}
	
	




}

void HelloWorld::IsPlayerNearby()
{
	
		float distanceX = abs(player_sprite->player_sprite->getPositionX() - ALLENEMIES.at(0)->Enemy_sprite->getPositionX());
		float distanceX2 = abs(player_sprite->player_sprite->getPositionX() - ALLENEMIES.at(1)->Enemy_sprite->getPositionX());
		float distanceX3 = abs(player_sprite->player_sprite->getPositionX() - ALLENEMIES.at(2)->Enemy_sprite->getPositionX());
		//CCLOG("%f", distanceX);
		//float distanceX = abs(player_sprite->player_sprite->getPositionX() - ALLENEMIES.at(1)->Enemy_sprite->getPositionX());
		//CCLOG("%f %f", distanceX, distanceY);
		if (distanceX < 300)
		{
			
			EnemyFollowCheck = 0;
		}

		else if (distanceX2<300)
		{

			
			EnemyFollowCheck = 1;
		}
		else if (distanceX3<300)
		{

			
			EnemyFollowCheck = 2;
		}
		else if (distanceX2>300&& distanceX>300&& distanceX3>300)
		{

			
			EnemyFollowCheck = -1;
		}

	

}

void HelloWorld::FollowPlayer(int i)
{
	if (ALLENEMIES.at(i)->dead == false)
	{
		ALLENEMIES.at(i)->Enemy_sprite->stopAllActionsByTag(99);
		if (ALLENEMIES.at(i)->Enemy_sprite->getPositionX() < player_sprite->player_sprite->getPositionX())
		{

			ALLENEMIES.at(i)->velocity_x = 3;
			ALLENEMIES.at(i)->Enemy_sprite->setFlippedX(true);
			ALLENEMIES.at(i)->Enemy_sprite->setPositionX(ALLENEMIES.at(i)->Enemy_sprite->getPositionX() + ALLENEMIES.at(i)->velocity_x);


		}
		else if (ALLENEMIES.at(i)->Enemy_sprite->getPositionX() > player_sprite->player_sprite->getPositionX())
		{

			ALLENEMIES.at(i)->velocity_x = -3;
			ALLENEMIES.at(i)->Enemy_sprite->setFlippedX(false);
			ALLENEMIES.at(i)->Enemy_sprite->setPositionX(ALLENEMIES.at(i)->Enemy_sprite->getPositionX() + ALLENEMIES.at(i)->velocity_x);
		}
	}
}
HelloWorld::HelloWorld(void)
{
	setKeyboardEnabled(true);
}

HelloWorld::~HelloWorld(void)
{
}

