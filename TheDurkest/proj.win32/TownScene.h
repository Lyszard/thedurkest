#pragma once

#include "cocos2d.h"
#include "TavernScene.h"
using namespace cocos2d;

class TownScene : public cocos2d::Layer
{
public:
	// there's no 'id' in cpp, so we recommend returning the class instance pointer
	static cocos2d::Scene* createScene();
	int gold;
	int PDMG;
	int EDMG;
	// Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
	virtual bool init();

	// a selector callback
	void menuCloseCallback(cocos2d::Ref* pSender);

	// implement the "static create()" method manually
	CREATE_FUNC(TownScene);

	cocos2d::Sprite *mySprite;

	void Play(Ref *pSender);
	void Highscores(Ref *pSender);
	void Settings(Ref *pSender);

	void ImageButton(Ref *pSender);
	
};

