#include "Enemy.h"
#include"parameters.h"
#include<vector>
#include <random>
using namespace cocos2d;

Enemy::Enemy(Layer * layer, Level * level,int position)
{
	left = true;
	velocity_x = 0;
	velocity_y = 0;
	health = 50;
	
	Enemy_sprite = Sprite::create("SkeletonA/enemy1-Walk_0.png");
	Enemy_sprite->setTag(66);
	Enemy_sprite->setZOrder(-1);
	Vector<SpriteFrame*> animFrames(17);
	char str[100] = { 0 };

	for (int i = 0; i < 17; i++)
	{
		sprintf(str, "SkeletonA/enemy1-Walk_%d.png", i);
		auto frame = SpriteFrame::create(str, Rect(0, 0, 600, 450));
		
		animFrames.pushBack(frame);
	}

	auto animation = Animation::createWithSpriteFrames(animFrames, 0.1f, true);
	auto animate = Animate::create(animation);
	Enemy_sprite->runAction(RepeatForever::create(animate));
	Enemy_sprite->setTextureRect(Rect(0, 0, 90, 90));
	Enemy_sprite->setScale(0.2);
	
	
	std::random_device rd;     // only used once to initialise (seed) engine
	std::mt19937 rng(rd());    // random-number engine used (Mersenne-Twister in this case)
	std::uniform_int_distribution<int> uni(0, 1); // guaranteed unbiased
	auto random_integer = uni(rng);
	CCLOG("%d", random_integer);
	//Point point = Point(RandomPosX[0], RandomPosY[0]);
	Size size = Enemy_sprite->getContentSize();
	Enemy_sprite->setPosition(position,140);
	

	Enemy_sprite->setFlippedX(true);
	layer->addChild(Enemy_sprite);


}
void Enemy::DeathAnimation()
{

	this->Enemy_sprite->stopAllActions();
	double position = this->Enemy_sprite->getPositionX();


	Vector<SpriteFrame*> animFrames(8);

	char str[100] = { 0 };

	for (int i = 0; i < 7; i++)
	{
		sprintf(str, "SkeletonA/Death/enemy1-Die_%d.png", i);
		auto frame = SpriteFrame::create(str, Rect(0, 0, 1000, 600));
		animFrames.pushBack(frame);
	}

	auto deathanimation = Animation::createWithSpriteFrames(animFrames, 0.05f, true);
	auto deathanimate = Animate::create(deathanimation);
	Enemy_sprite->runAction(Repeat::create(deathanimate, 1));
	//player_sprite->runAction(RepeatForever::create(deathanimate));

}
void Enemy::MoveEnemy()
{
	
	if (left == true)
	{
		auto action = MoveBy::create(1, Point(-200, 0));
		action->setTag(99);
		left = false;
		right = true;
		Enemy_sprite->setFlippedX(false);
		Enemy_sprite->runAction(action);
		
	}
	else if (right == true)
	{
		auto action = MoveBy::create(1, Point(200, 0));
		action->setTag(99);
		left = true;
		right = false;
		Enemy_sprite->setFlippedX(true);
		Enemy_sprite->runAction(action);
	}
}

Enemy::~Enemy()
{
}
