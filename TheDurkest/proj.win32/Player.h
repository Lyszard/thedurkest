#pragma once
#include "cocos2d.h"
#include "Level.h"
#include <vector>

using namespace cocos2d;
class Player : public Sprite
{
public:
	Player(Layer * layer, Level * level);
	~Player();

	
	bool right;
	bool atack;
	float velocity_x;
	float velocity_y;

	int health;

	bool AnimProhib;
	int YPosCounter;

	void MovePlayer(float dt);
	void Atack(Layer * layer);
	void IdleAnimation(Layer* layer);
	void AttackAnimation(Layer*layer);
	void RunAnimation(Layer * layer);
	void DeathAnimation();
	
	Sprite * atack_sprite;
	Rect * PlayerRect;
	Sprite * player_sprite;
	
};
