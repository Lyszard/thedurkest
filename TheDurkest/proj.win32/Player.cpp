#include "Player.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"
#include "parameters.h"
#include "Level.h"
#include <vector>
using namespace cocos2d;


Player::Player(Layer * layer, Level * level)
{
	
	right = true;
	atack = false;
	velocity_x = 0;
	velocity_y = 0;
	health = 100;
	AnimProhib = false;

	
/////////////////////////////////////////////////////////////////////
	
	player_sprite = Sprite::create("PlayerA/Idle/Player-Idle_0.png");
	player_sprite->setTag(25);
	Vector<SpriteFrame*> animFrames(8);
	
	char str[100] = { 0 };

	for (int i = 0; i <= 7; i++)
	{
		sprintf(str, "PlayerA/Idle/Player-Idle_%d.png", i);
		auto frame = SpriteFrame::create(str, Rect(0, 0, 600, 450));
		animFrames.pushBack(frame);
	}

	auto idleanimation = Animation::createWithSpriteFrames(animFrames, 0.15f, true);
	auto idleanimate = Animate::create(idleanimation);
	player_sprite->runAction(RepeatForever::create(idleanimate));
	



	//////////////////////////////////////////////////////////////
	player_sprite->setScale(0.2);
	Point point = Point(0, 0);
	Size size = player_sprite->getContentSize();
	player_sprite->setPosition(200,140);
	player_sprite->setFlippedX(true);
	
	
	player_sprite->setZOrder(-1);
	layer->addChild(player_sprite);



	/******************************************************************************/
	



	/****************************************************************************/
}



void Player::MovePlayer(float dt)
{
	player_sprite->setPositionX(player_sprite->getPositionX() + this->velocity_x);
	
	
	if (right)
	{
		
		player_sprite->setFlippedX(true);
		
		
	}
	if (!right)
	{
		
		player_sprite->setFlippedX(false);
		

	}



}





void Player::Atack(Layer * layer)
{
	
	atack_sprite = Sprite::create("slashv3.png",Rect(0,0,203,200));
	atack_sprite->setScale(0.3);
	atack_sprite->setTag(9);
	atack_sprite->setGlobalZOrder(1);
	atack_sprite->setTextureRect(Rect(0, 0, 203 ,200));
	
	
	
	
		if (this->right)
		{
		
			atack_sprite->setPositionX(player_sprite->getPositionX() + 50);
			atack_sprite->setPositionY(player_sprite->getPositionY());
			
			atack_sprite->setFlippedX(false);

		}
		else
		{

			atack_sprite->setPositionX(player_sprite->getPositionX() - 50);
			atack_sprite->setPositionY(player_sprite->getPositionY());
			atack_sprite->setFlippedX(true);
		}
		layer->addChild(atack_sprite);
	
	

}
void Player::DeathAnimation()
{

	this->player_sprite->stopAllActions();
	double position = this->player_sprite->getPositionX();


	Vector<SpriteFrame*> animFrames(14);

	char str[100] = { 0 };

	for (int i = 0; i < 13; i++)
	{
		sprintf(str, "PlayerA/Death/Player-Die_%d.png", i);
		auto frame = SpriteFrame::create(str, Rect(0, 0, 1000,600));
		animFrames.pushBack(frame);
	}

	auto deathanimation = Animation::createWithSpriteFrames(animFrames, 0.05f, true);
	auto deathanimate = Animate::create(deathanimation);
	player_sprite->runAction(Repeat::create(deathanimate,1));
	//player_sprite->runAction(RepeatForever::create(deathanimate));

}
void Player::RunAnimation(Layer * layer)
{
	
		this->player_sprite->stopAllActions();
		double position = this->player_sprite->getPositionX();
		
		
		Vector<SpriteFrame*> animFrames(17);
		
		char str[100] = { 0 };

		for (int i = 1; i <= 17; i++)
		{
			sprintf(str, "PlayerA/Player-Run_%d.png", i);
			auto frame = SpriteFrame::create(str, Rect(0, 0, 600, 450));
			animFrames.pushBack(frame);
		}

		auto runanimation = Animation::createWithSpriteFrames(animFrames, 0.05f, true);
		auto runanimate = Animate::create(runanimation);
		player_sprite->runAction(RepeatForever::create(runanimate));
	
	

	

}



void Player::IdleAnimation(Layer * layer)
{
	
		this->player_sprite->stopAllActions();
		

		Vector<SpriteFrame*> animFrames(8);
		
		char str[100] = { 0 };

		for (int i = 0; i <= 7; i++)
		{
			sprintf(str, "PlayerA/Idle/Player-Idle_%d.png", i);
			auto frame = SpriteFrame::create(str, Rect(0, 0, 600, 450));
			animFrames.pushBack(frame);
		}

		auto idleanimation = Animation::createWithSpriteFrames(animFrames, 0.15f, true);
		auto idleanimate = Animate::create(idleanimation);
		player_sprite->runAction(RepeatForever::create(idleanimate));
	


}


void Player::AttackAnimation(Layer * layer)
{

	this->player_sprite->stopAllActions();


	Vector<SpriteFrame*> animFrames(6);

	char str[100] = { 0 };

	for (int i = 0; i <= 5; i++)
	{
		sprintf(str, "PlayerA/Attack/Player-Attack_%d.png", i);
		auto frame = SpriteFrame::create(str, Rect(0, 0, 600, 450));
		animFrames.pushBack(frame);
	}

	auto attackanimation = Animation::createWithSpriteFrames(animFrames, 0.03f, true);
	auto attackanimate = Animate::create(attackanimation);
	player_sprite->runAction(RepeatForever::create(Repeat::create(attackanimate,1)));
	



}
Player::~Player() {}



