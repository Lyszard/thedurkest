#pragma once
#include "cocos2d.h"
#include"Level.h"
using namespace cocos2d;
class Enemy :public Sprite
{
public:
	Enemy(Layer * layer, Level * level, int position);
	~Enemy();

	Sprite * Enemy_sprite;
	bool left;
	bool dead;

	bool right;
	int velocity_x;
	int velocity_y;
	int health;
	void MoveEnemy();
	void DeathAnimation();
	
};

