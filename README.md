Uprzejmię przepraszam za opóźnienie prototypu mojego projektu, czego powodem były następujące 
problemy techniczne:
- projekt cocosa tworzy 2 główne foldery z których korzysta Visual Studio:
 * Classes
 * Win32
przy czym, gdy tworzymy jakąś nową klasę wewnątrz Visual Studio, nasze pliki zostają dodane tylko do folderu Win32,
przez co przy kompilacji wyskakują błędy,że nasze pliki nie istnieją. Należy wtedy ręcznie kopiować pliki klas do folderu Classes,
dopiero wtedy przy komppilacji wszystko działa.
- przy jakiejkolwiek zmianie w pliku nagłówkowym klasy i kompilacji pojawiają się błędy,że np. nowo dodane metody nie istnieją lub 
nie należą do naszej klasy, koniecznością staje się na nowo ręczne kopiowanie plików klasy z folderu Win34 do folderu Classes.
- Przy tworzeniu np wskaznika jako składową jednej klasy i poprawnej jego inicjalizacji w konstruktorze, przy kompilacji może pojawić się
problem w losowym miejscu w innej losowej klasie, po czym po np kilku kompilacjach problem samoistnie znika lub pojawia się na nowo.
- losowe ciągłe problemy z dostępem do plików biblioteki cocosa z rozszerzeniem .pdb, jak w powyższym przykładzie problem samoistnie pojawia 
się i znika przy losowych próbach kompilacji.
- przy utworzeniu nowego projektu cocosa i skopiowaniu kodu źródłowego z poprzedniego projektu ten sam kod potrafi działać
zupełnie inaczej.